FROM openjdk:18-jdk-oraclelinux8

ENV DB_HOST postgres-server
ENV DB_NAME root
ENV DB_USERNAME: root
ENV DB_PASSWORD: password

ARG JAR_FILE=build/libs/wallet-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","-XX:+UseSerialGC","-Xss512k","-XX:MaxRAM=256m","/app.jar"]
