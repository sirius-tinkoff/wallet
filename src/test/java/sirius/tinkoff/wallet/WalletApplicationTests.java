package sirius.tinkoff.wallet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WalletApplicationTests {

    @Test
    void checkDateFormat() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date parse = simpleDateFormat.parse("2021-08-25T09:24:59");
        System.out.println(parse);

        String strDate = simpleDateFormat.format(parse);
        System.out.println(strDate);
    }

}
