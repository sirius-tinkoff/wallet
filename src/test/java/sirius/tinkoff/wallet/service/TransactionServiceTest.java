package sirius.tinkoff.wallet.service;

import java.text.ParseException;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import sirius.tinkoff.wallet.WalletApplicationTests;
import sirius.tinkoff.wallet.converter.DateConverter;
import sirius.tinkoff.wallet.dao.CurrencyType;
import sirius.tinkoff.wallet.dao.Transaction;
import sirius.tinkoff.wallet.dao.TransactionType;
import sirius.tinkoff.wallet.dto.TransactionCreationDto;
import sirius.tinkoff.wallet.repository.TransactionRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransactionServiceTest extends WalletApplicationTests {
    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    public void testCreateTransaction() throws ParseException {
        String date = "2021-08-25T17:09:09";
        TransactionCreationDto transaction = new TransactionCreationDto()
                .setWalletId(100001L)
                .setAmount("345")
                .setTransactionType(TransactionType.INCOME)
                .setCategoryId(1L)
                .setDate(date)
                .setCurrencyType(CurrencyType.RUB);

        TransactionCreationDto transactionCreationDto = transactionService.create(transaction);
        assertEquals(date, transactionCreationDto.getDate());

        Optional<Transaction> transactionOpt = transactionRepository.findById(transactionCreationDto.getId());
        assertTrue(transactionOpt.isPresent());
        Date transactionDate = transactionOpt.get().getDate();
        assertEquals(date, DateConverter.dateToString(transactionDate));
    }

}
