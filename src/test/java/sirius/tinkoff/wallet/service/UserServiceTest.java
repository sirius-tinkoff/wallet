package sirius.tinkoff.wallet.service;

import java.text.ParseException;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import sirius.tinkoff.wallet.WalletApplicationTests;
import sirius.tinkoff.wallet.dao.User;
import sirius.tinkoff.wallet.dto.UserDto;
import sirius.tinkoff.wallet.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserServiceTest extends WalletApplicationTests {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testCreateUser() throws ParseException {
        String email = "test@email";
        UserDto user = new UserDto()
                .setGoogleToken("TeStToKeN")
                .setEmail("test@email");

        UserDto userDto = userService.create(user);
        assertEquals(email, user.getEmail());

        Optional<User> userOpt = userRepository.findById(userDto.getId());
        assertTrue(userOpt.isPresent());
        String emailTest = userOpt.get().getEmail();
        assertEquals(email, emailTest);
    }
}
