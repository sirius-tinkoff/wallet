package sirius.tinkoff.wallet.dao;

public enum TransactionType {
    INCOME, OUTCOME
}
