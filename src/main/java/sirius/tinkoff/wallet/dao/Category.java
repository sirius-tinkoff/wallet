package sirius.tinkoff.wallet.dao;

import java.util.Date;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@Accessors(chain = true)
@SequenceGenerator(name = "category_seq", sequenceName = "category_seq", allocationSize = 1)
public class Category {
    @Id
    @GeneratedValue(generator = "category_seq")
    private Long id;

    private String name;
    private Long icon;
    private Long color;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="creation_date")
    private Date creationDate;

    @Enumerated(EnumType.STRING)
    @Column(name="category_type")
    private TransactionType categoryType;

    @Column(name="user_id")
    private Long userId;
}
