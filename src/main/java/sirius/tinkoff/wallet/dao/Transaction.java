package sirius.tinkoff.wallet.dao;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
@SequenceGenerator(name = "transaction_seq", sequenceName = "transaction_seq", allocationSize = 1)
public class Transaction {
    @Id
    @GeneratedValue(generator = "transaction_seq")
    private Long id;

    @Column(name = "wallet_id")
    private long walletId;

    private BigDecimal amount;

    @Column(name = "category_id")
    private long categoryId;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type")
    private TransactionType transactionType;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency_type")
    private CurrencyType currencyType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "transaction_date")
    private Date date;

    private BigDecimal rate = new BigDecimal("1");
}
