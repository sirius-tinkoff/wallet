package sirius.tinkoff.wallet.dao;

public enum CurrencyType {
    RUB, USD, EUR
}
