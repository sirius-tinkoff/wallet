package sirius.tinkoff.wallet.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity(name = "app_user")
@Getter
@Setter
@ToString(exclude = "walletList")
@Accessors(chain = true)
@NoArgsConstructor
@SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 1)
public class User {
    @Id
    @GeneratedValue(generator = "user_seq")
    private Long id;

    @Column(name = "google_token")
    private String googleToken;

    private String email;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private List<Wallet> walletList;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_entrance")
    private Date lastEntrance;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registration_date")
    private Date registrationDate;
}
