package sirius.tinkoff.wallet.controller;

import java.text.ParseException;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sirius.tinkoff.wallet.dto.UserDto;
import sirius.tinkoff.wallet.dto.UserSummaryInfoDto;
import sirius.tinkoff.wallet.service.TransactionService;
import sirius.tinkoff.wallet.service.UserService;

import javax.validation.Valid;

@RequestMapping("/user")
@RestController
@RequiredArgsConstructor
public class UserController {
    private final TransactionService transactionService;
    private final UserService userService;

    @Operation(summary = "Получить пользователя по Google ID.")
    @GetMapping(value = "/googleId/", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto getUserByGoogleId(@RequestHeader String googleToken) {
        return userService.getUserByGoogleToken(googleToken);
    }

    @Operation(summary = "Получить пользователя по email.")
    @GetMapping(value = "/email/", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto getUserByEmail(@RequestHeader String email) {
        return userService.getUserByEmail(email);
    }

    @Operation(summary = "Получить итоговую информацию по всем кошелькам для пользователя.")
    @GetMapping(value = "/info/", produces =  MediaType.APPLICATION_JSON_VALUE)
    public UserSummaryInfoDto getUserInfoById (@RequestHeader Long id) {
        return userService.getUserSummaryInfoById(id);
    }

    @Operation(summary = "Добавить(создать) пользователя.")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto createUser(@Valid @RequestBody UserDto userDto) throws ParseException {
        return userService.create(userDto);
    }

    @Operation(summary = "Изменить пользователя.")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto updateUser(@Valid @RequestBody UserDto userDto) throws ParseException {
        return userService.update(userDto);
    }

    @Operation(summary = "Удалить пользователя по id.")
    @DeleteMapping(value = "/")
    public void deleteUser(@RequestHeader long id) {
        userService.deleteById(id);
    }
}
