package sirius.tinkoff.wallet.controller;

import java.text.ParseException;

import javax.validation.Valid;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sirius.tinkoff.wallet.dto.TransactionCreationDto;
import sirius.tinkoff.wallet.dto.TransactionDto;
import sirius.tinkoff.wallet.service.TransactionService;

@RequestMapping("/transaction")
@RestController
@RequiredArgsConstructor
public class TransactionController {
    private final TransactionService transactionService;

    @Operation(summary = "Получить транзакцию по id.")
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public TransactionDto getTransaction(@RequestHeader Long id, @RequestHeader String googleToken) {
        return transactionService.getById(id);
    }

    @Operation(summary = "Добавить(создать) транзакцию.")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TransactionCreationDto createTransaction(
            @Valid @RequestBody TransactionCreationDto transactionCreationDto) throws ParseException {
        return transactionService.create(transactionCreationDto);
    }

    @Operation(summary = "Изменить транзакцию.")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TransactionCreationDto updateTransaction(
            @Valid @RequestBody TransactionCreationDto transactionCreationDto) throws ParseException {
        return transactionService.update(transactionCreationDto);
    }

    @Operation(summary = "Удалить транзакцию по id.")
    @DeleteMapping(value = "/")
    public void deleteTransaction(@RequestHeader long id) {
        transactionService.deleteById(id);
    }
}
