package sirius.tinkoff.wallet.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sirius.tinkoff.wallet.dto.WalletCreationDto;
import sirius.tinkoff.wallet.dto.WalletDetailsDto;
import sirius.tinkoff.wallet.service.WalletService;

import javax.validation.Valid;

@RequestMapping("/wallet")
@RestController
@RequiredArgsConstructor
public class WalletController {
    private final WalletService walletService;

    @Operation(summary = "Получить краткую информацию о кошельке.")
    @GetMapping(value = "/short/", produces = MediaType.APPLICATION_JSON_VALUE)
    public WalletCreationDto getWallet(@RequestHeader Long id) {
        return walletService.getById(id);
    }

    @Operation(summary = "Получить детали кошелька.")
    @GetMapping(value="/details/", produces = MediaType.APPLICATION_JSON_VALUE)
    public WalletDetailsDto getWalletDetails(@RequestHeader Long id) {
        return walletService.getWalletInfoById(id);
    }

    @Operation(summary = "Добавить(создать) кошелёк.")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public WalletCreationDto createWallet(@Valid @RequestBody WalletCreationDto walletCreationDto) {
        return walletService.create(walletCreationDto);
    }

    @Operation(summary = "Обновить кошелёк.")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public WalletCreationDto updateWallet(@Valid @RequestBody WalletCreationDto walletCreationDto) {
        return walletService.update(walletCreationDto);
    }

    @Operation(summary = "Удалить кошелёк.")
    @DeleteMapping(value = "/")
    public void deleteWallet(@RequestHeader Long id) {
        walletService.deleteById(id);
    }
}
