package sirius.tinkoff.wallet.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sirius.tinkoff.wallet.dto.CategoryDto;
import sirius.tinkoff.wallet.service.CategoryService;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/category")
@RestController
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @Operation(summary = "Получить список категорий по id пользователя.")
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoryDto> getCategoriesByUserId(@RequestHeader Long id) {
        return categoryService.getCategoriesByUserId(id);
    }

    @Operation(summary = "Получить список \"прибыльных\" категорий по id пользователя.")
    @GetMapping(value = "/income", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoryDto> getIncomeCategoriesByUserId(@RequestHeader Long id) {
        return categoryService.getIncomeCategoriesByUserId(id);
    }

    @Operation(summary = "Получить список \"тратящих\" категорий по id пользователя.")
    @GetMapping(value = "/outcome", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoryDto> getOutcomeCategoriesByUserId(@RequestHeader Long id) {
        return categoryService.getOutcomeCategoriesByUserId(id);
    }

    @Operation(summary = "Создать категорию.")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoryDto createCategory(@Valid @RequestBody CategoryDto categoryDto) {
        return categoryService.create(categoryDto);
    }

    @Operation(summary = "Изменить категорию.")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoryDto updateCategory(@Valid @RequestBody CategoryDto categoryDto) {
        return categoryService.update(categoryDto);
    }

    @Operation(summary = "Удалить категорию.")
    @DeleteMapping(value = "/")
    public void deleteCategory(@RequestHeader Long id) {
        categoryService.deleteById(id);
    }
}
