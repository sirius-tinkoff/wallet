package sirius.tinkoff.wallet.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "spring.jpa")
@Getter
@Setter
public class JpaProp {
//    spring.jpa.database-platform = org.hibernate.dialect.PostgresqlDialect
//    spring.jpa.hibernate.ddl-auto=none
    private String databasePlatform;
    private String ddlAuto;

}
