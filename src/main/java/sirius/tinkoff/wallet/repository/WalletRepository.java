package sirius.tinkoff.wallet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sirius.tinkoff.wallet.dao.Wallet;

import java.util.List;

@Repository
public interface WalletRepository extends CrudRepository<Wallet, Long> {
    List<Wallet> getWalletsByUserId(Long userId);
}
