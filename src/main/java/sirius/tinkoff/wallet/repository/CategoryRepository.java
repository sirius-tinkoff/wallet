package sirius.tinkoff.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sirius.tinkoff.wallet.dao.Category;
import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Query(value = "SELECT * FROM category WHERE (user_id=:userId OR user_id IS null)", nativeQuery = true)
    List<Category> getCategoriesByUserId(Long userId);

    @Query(value = "SELECT * FROM category " +
                   "WHERE category_type = 'INCOME' AND (user_id=:userId OR user_id IS null)",
           nativeQuery = true)
    List<Category> getIncomeCategoriesByUserId(@Param("userId") Long userId);

    @Query(value = "SELECT * FROM category " +
                   "WHERE category_type = 'OUTCOME' AND (user_id=:userId OR user_id IS null)",
           nativeQuery = true)
    List<Category> getOutcomeCategoriesByUserId(@Param("userId") Long userId);

}
