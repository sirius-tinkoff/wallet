package sirius.tinkoff.wallet.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sirius.tinkoff.wallet.dao.Transaction;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    List<Transaction> getByWalletId(@Param("walletId") Long walletId);

    @Query(value = "SELECT COALESCE(SUM(amount), 0) FROM transaction " +
                   "WHERE wallet_id=:walletId AND transaction_type='INCOME'", nativeQuery = true)
    BigDecimal getSumIncomeByWallet(@Param("walletId") Long walletId);

    @Query(value = "SELECT COALESCE(SUM(amount), 0) FROM transaction " +
                   "WHERE wallet_id=:walletId AND transaction_type='OUTCOME'", nativeQuery = true)
    BigDecimal getSumOutcomeByWallet(@Param("walletId") Long walletId);

    @Query(value="SELECT COALESCE(SUM(amount), 0) " +
                 "FROM (transaction LEFT JOIN wallet ON transaction.wallet_id = wallet.id) AS j " +
                 "WHERE j.user_id=:userId AND j.transaction_type='INCOME'", nativeQuery = true)
    BigDecimal getSumIncomeByUser(@Param("userId") Long userId);

    @Query(value="SELECT COALESCE(SUM(amount), 0) " +
                 "FROM (transaction LEFT JOIN wallet ON transaction.wallet_id = wallet.id) AS j " +
                 "WHERE j.user_id=:userId AND j.transaction_type = 'OUTCOME'", nativeQuery = true)
    BigDecimal getSumOutcomeByUser(@Param("userId") Long userId);

    @Transactional
    void deleteAllByWalletId(Long walletId);
}
