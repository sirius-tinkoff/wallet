package sirius.tinkoff.wallet.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.User;
import sirius.tinkoff.wallet.dto.UserDto;

@Component
public class UserToUserDtoConverter {
    public UserDto convert(User user) {
        return new UserDto()
                .setId(user.getId())
                .setGoogleToken(user.getGoogleToken())
                .setEmail(user.getEmail());
    }
}
