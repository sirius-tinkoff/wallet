package sirius.tinkoff.wallet.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Wallet;
import sirius.tinkoff.wallet.dto.WalletDto;

import java.math.BigDecimal;
import java.util.Objects;

@Component
public class WalletDtoToWalletConverter {
    public Wallet convert(WalletDto walletDto) {
        BigDecimal limit;
        if (Objects.equals(walletDto.getLimit(), "")) {
            limit = null;
        } else {
            limit = new BigDecimal(walletDto.getLimit());
        }

        return new Wallet()
                .setId(walletDto.getId())
                .setName(walletDto.getName())
                .setBalance(new BigDecimal(walletDto.getBalance()))
                .setLimit(limit)
                .setCurrencyType(walletDto.getCurrencyType())
                .setHidden(walletDto.isHidden());
    }
}
