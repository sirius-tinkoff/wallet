package sirius.tinkoff.wallet.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Category;
import sirius.tinkoff.wallet.dto.CategoryDto;

@Component
public class CategoryToCategoryDtoConverter {
    public CategoryDto convert(Category category) {
        return new CategoryDto()
                .setId(category.getId())
                .setName(category.getName())
                .setColor(category.getColor())
                .setIcon(category.getIcon())
                .setCreationDate(category.getCreationDate())
                .setCategoryType(category.getCategoryType())
                .setUserId(category.getUserId());
    }
}
