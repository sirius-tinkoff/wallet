package sirius.tinkoff.wallet.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Wallet;
import sirius.tinkoff.wallet.dto.WalletCreationDto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Component
public class WalletCreationDtoToWalletConverter {
    public Wallet convert(WalletCreationDto walletCreationDto) {
        BigDecimal limit;
        if (Objects.equals(walletCreationDto.getLimit(), "")) {
            limit = null;
        } else {
            limit = new BigDecimal(walletCreationDto.getLimit());
        }

        return new Wallet()
                .setId(walletCreationDto.getId())
                .setName(walletCreationDto.getName())
                .setBalance(new BigDecimal(walletCreationDto.getBalance()))
                .setLimit(limit)
                .setCurrencyType(walletCreationDto.getCurrencyType())
                .setHidden(walletCreationDto.getHidden());
    }
}
