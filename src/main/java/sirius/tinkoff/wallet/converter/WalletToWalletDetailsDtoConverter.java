package sirius.tinkoff.wallet.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Wallet;
import sirius.tinkoff.wallet.dto.WalletDetailsDto;

@Component
public class WalletToWalletDetailsDtoConverter {
    public WalletDetailsDto convert(Wallet wallet) {
        String limit;
        if (wallet.getLimit() == null) {
            limit = "";
        } else {
            limit = wallet.getLimit().toString();
        }

        return new WalletDetailsDto()
                .setId(wallet.getId())
                .setName(wallet.getName())
                .setLimit(limit)
                .setCurrencyType(wallet.getCurrencyType())
                .setHidden(wallet.isHidden());
    }
}
