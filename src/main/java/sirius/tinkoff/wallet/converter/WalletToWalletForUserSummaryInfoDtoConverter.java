package sirius.tinkoff.wallet.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Wallet;
import sirius.tinkoff.wallet.dto.WalletForUserSummaryDto;

@Component
public class WalletToWalletForUserSummaryInfoDtoConverter {
    public WalletForUserSummaryDto convert(Wallet wallet) {
        String limit;
        if (wallet.getLimit() == null) {
            limit = "";
        } else {
            limit = wallet.getLimit().toString();
        }

        return new WalletForUserSummaryDto()
                .setId(wallet.getId())
                .setName(wallet.getName())
                .setLimit(limit)
                .setCurrencyType(wallet.getCurrencyType())
                .setIsHidden(wallet.isHidden());
    }
}
