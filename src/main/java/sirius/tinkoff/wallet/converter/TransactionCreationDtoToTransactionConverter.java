package sirius.tinkoff.wallet.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Transaction;
import sirius.tinkoff.wallet.dto.TransactionCreationDto;

import java.math.BigDecimal;
import java.text.ParseException;

@Component
public class TransactionCreationDtoToTransactionConverter {
    public Transaction convert(TransactionCreationDto transactionCreationDto) throws ParseException {
        return new Transaction()
                .setId(transactionCreationDto.getId())
                .setWalletId(transactionCreationDto.getWalletId())
                .setAmount(new BigDecimal(transactionCreationDto.getAmount()))
                .setCategoryId(transactionCreationDto.getCategoryId())
                .setTransactionType(transactionCreationDto.getTransactionType())
                .setCurrencyType(transactionCreationDto.getCurrencyType())
                .setDate(DateConverter.stringToDate(transactionCreationDto.getDate()));
    }
}
