package sirius.tinkoff.wallet.converter;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Transaction;
import sirius.tinkoff.wallet.dto.TransactionDto;

@Component
public class TransactionToTransactionDtoConverter {
    public TransactionDto convert(Transaction transaction) {
        return new TransactionDto()
                .setId(transaction.getId())
                .setAmount(transaction.getAmount().toString())
                .setTransactionType(transaction.getTransactionType())
                .setDate(DateConverter.dateToString(transaction.getDate()))
                .setCurrencyType(transaction.getCurrencyType());
    }
}
