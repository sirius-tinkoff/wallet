package sirius.tinkoff.wallet.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Wallet;
import sirius.tinkoff.wallet.dto.WalletDto;

@Component
public class WalletToWalletDtoConverter {
    public WalletDto convert(Wallet wallet) {
        String limit;
        if (wallet.getLimit() == null) {
            limit = "";
        } else {
            limit = wallet.getLimit().toString();
        }

        return new WalletDto()
                .setId(wallet.getId())
                .setUserId(wallet.getUser().getId())
                .setName(wallet.getName())
                .setBalance(wallet.getBalance().toString())
                .setLimit(limit)
                .setCurrencyType(wallet.getCurrencyType())
                .setHidden(wallet.isHidden());
    }
}
