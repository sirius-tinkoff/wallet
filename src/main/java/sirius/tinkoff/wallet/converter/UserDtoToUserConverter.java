package sirius.tinkoff.wallet.converter;

import java.text.ParseException;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.User;
import sirius.tinkoff.wallet.dto.UserDto;

@Component
public class UserDtoToUserConverter {
    public User convert (UserDto userDto) throws ParseException {
        return new User()
                .setId(userDto.getId())
                .setGoogleToken(userDto.getGoogleToken())
                .setEmail(userDto.getEmail());
    }
}
