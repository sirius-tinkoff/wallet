package sirius.tinkoff.wallet.converter;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Transaction;
import sirius.tinkoff.wallet.dto.TransactionDto;

@Component
public class TransactionDtoToTransactionConverter {
    public Transaction convert(TransactionDto dto) throws ParseException {
        return new Transaction()
                .setId(dto.getId())
                .setAmount(new BigDecimal(dto.getAmount()))
                .setTransactionType(dto.getTransactionType())
                .setCategoryId(dto.getCategory().getId())
                .setDate(DateConverter.stringToDate(dto.getDate()))
                .setCurrencyType(dto.getCurrencyType());
    }
}
