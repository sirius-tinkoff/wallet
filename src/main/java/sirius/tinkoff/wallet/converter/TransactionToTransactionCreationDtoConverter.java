package sirius.tinkoff.wallet.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Transaction;
import sirius.tinkoff.wallet.dto.TransactionCreationDto;

@Component
public class TransactionToTransactionCreationDtoConverter {
    public TransactionCreationDto convert(Transaction transaction) {
        return new TransactionCreationDto()
                .setId(transaction.getId())
                .setWalletId(transaction.getWalletId())
                .setAmount(transaction.getAmount().toString())
                .setTransactionType(transaction.getTransactionType())
                .setCategoryId(transaction.getCategoryId())
                .setDate(DateConverter.dateToString(transaction.getDate()))
                .setCurrencyType(transaction.getCurrencyType());
    }
}
