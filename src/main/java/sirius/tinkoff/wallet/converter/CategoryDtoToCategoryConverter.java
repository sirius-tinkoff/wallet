package sirius.tinkoff.wallet.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.wallet.dao.Category;
import sirius.tinkoff.wallet.dto.CategoryDto;

@Component
public class CategoryDtoToCategoryConverter {
    public Category convert(CategoryDto categoryDto) {
        return new Category()
                .setId(categoryDto.getId())
                .setName(categoryDto.getName())
                .setColor(categoryDto.getColor())
                .setIcon(categoryDto.getIcon())
                .setCreationDate(categoryDto.getCreationDate())
                .setCategoryType(categoryDto.getCategoryType())
                .setUserId(categoryDto.getUserId());
    }
}
