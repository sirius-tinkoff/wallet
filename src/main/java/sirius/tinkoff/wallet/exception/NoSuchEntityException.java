package sirius.tinkoff.wallet.exception;

import java.util.NoSuchElementException;

public class NoSuchEntityException extends NoSuchElementException {
    public NoSuchEntityException() {
    }

    public NoSuchEntityException(String s) {
        super(s);
    }
}
