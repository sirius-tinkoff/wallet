package sirius.tinkoff.wallet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sirius.tinkoff.wallet.converter.CategoryDtoToCategoryConverter;
import sirius.tinkoff.wallet.converter.CategoryToCategoryDtoConverter;
import sirius.tinkoff.wallet.dao.Category;
import sirius.tinkoff.wallet.dto.CategoryDto;
import sirius.tinkoff.wallet.exception.NoSuchEntityException;
import sirius.tinkoff.wallet.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private CategoryDtoToCategoryConverter categoryDtoToCategoryConverter;
    @Autowired
    private CategoryToCategoryDtoConverter categoryToCategoryDtoConverter;

    public CategoryDto getCategoryById(Long id) {
        Category category = categoryRepository
                .findById(id)
                .orElseThrow(() -> new NoSuchEntityException(format("No category with such id (%d).", id)));
        return categoryToCategoryDtoConverter.convert(category);
    }

    public List<CategoryDto> getCategoriesByUserId(Long id) {
        List<Category> categories = categoryRepository.getCategoriesByUserId(id);
        List<CategoryDto> categoryDtoList = new ArrayList<>();
        for (Category category : categories) {
            categoryDtoList.add(categoryToCategoryDtoConverter.convert(category));
        }
        return categoryDtoList;
    }

    public List<CategoryDto> getIncomeCategoriesByUserId(Long id) {
        List<Category> categories = categoryRepository.getIncomeCategoriesByUserId(id);
        List<CategoryDto> categoryDtoList = new ArrayList<>();
        for (Category category : categories) {
                categoryDtoList.add(categoryToCategoryDtoConverter.convert(category));
        }
        return categoryDtoList;
    }

    public List<CategoryDto> getOutcomeCategoriesByUserId(Long id) {
        List<Category> categories = categoryRepository.getOutcomeCategoriesByUserId(id);
        List<CategoryDto> categoryDtoList = new ArrayList<>();
        for (Category category : categories) {
                categoryDtoList.add(categoryToCategoryDtoConverter.convert(category));
        }
        return categoryDtoList;
    }

    public CategoryDto create(CategoryDto categoryDto) {
        Category category = categoryDtoToCategoryConverter.convert(categoryDto);
        Category saved = categoryRepository.save(category);
        return categoryToCategoryDtoConverter.convert(saved);
    }

    public CategoryDto update(CategoryDto categoryDto) {
        Category category = categoryDtoToCategoryConverter.convert(categoryDto);
        categoryRepository.save(category);
        return categoryDto;
    }

    public void deleteById(Long id) {
        categoryRepository.findById(id).orElseThrow(() -> new NoSuchEntityException(format(
                "Cannot delete. No category with such id (%d).", id)));
        categoryRepository.deleteById(id);
    }
}
