package sirius.tinkoff.wallet.service;

import java.text.ParseException;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sirius.tinkoff.wallet.converter.UserDtoToUserConverter;
import sirius.tinkoff.wallet.converter.UserToUserDtoConverter;
import sirius.tinkoff.wallet.converter.WalletToWalletForUserSummaryInfoDtoConverter;
import sirius.tinkoff.wallet.dao.User;
import sirius.tinkoff.wallet.dao.Wallet;
import sirius.tinkoff.wallet.dto.UserDto;
import sirius.tinkoff.wallet.dto.UserSummaryInfoDto;
import sirius.tinkoff.wallet.dto.WalletForUserSummaryDto;
import sirius.tinkoff.wallet.exception.NoSuchEntityException;
import sirius.tinkoff.wallet.repository.TransactionRepository;
import sirius.tinkoff.wallet.repository.UserRepository;
import sirius.tinkoff.wallet.repository.WalletRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final TransactionRepository transactionRepository;
    private final WalletRepository walletRepository;

    private final UserDtoToUserConverter userDtoToUserConverter;
    private final UserToUserDtoConverter userToUserDtoConverter;

    public UserDto getUserByGoogleToken(String googleToken) {
        User user = userRepository
            .findByGoogleToken(googleToken)
            .orElseThrow(()-> new NoSuchEntityException(format("No user with such token (%s).", googleToken)));
        return userToUserDtoConverter.convert(user);
    }

    public UserDto getUserByEmail(String email) {
        User user = userRepository
                .findByEmail(email)
                .orElseThrow(() -> new NoSuchEntityException(format("No user with such email (%s).", email)));
        return userToUserDtoConverter.convert(user);
    }

    public UserDto create(UserDto userDto) throws ParseException {
        User user = userDtoToUserConverter.convert(userDto);
        user.setLastEntrance(new Date());
        user.setRegistrationDate(new Date());
        User saved = userRepository.save(user);
        return userToUserDtoConverter.convert(saved);
    }

    public UserDto update(UserDto userDto) throws ParseException {
        User user = userDtoToUserConverter.convert(userDto);
        User old = userRepository.findById(userDto.getId()).orElseThrow(
                () -> new NoSuchEntityException(format("No user with such id (%d).", userDto.getId())));
        user.setLastEntrance(old.getLastEntrance());
        user.setRegistrationDate(old.getRegistrationDate());
        userRepository.save(user);
        return userDto;
    }

    public void deleteById(Long id) {
        userRepository.findById(id).orElseThrow(() -> new NoSuchEntityException(
                format("Cannot delete. No user with such id (%d).", id)));
        userRepository.deleteById(id);
    }

    public UserSummaryInfoDto getUserSummaryInfoById(Long id) {
        BigDecimal sumIncome = transactionRepository.getSumIncomeByUser(id);
        BigDecimal sumOutcome = transactionRepository.getSumOutcomeByUser(id);
        BigDecimal balance = sumIncome.subtract(sumOutcome);
        List<Wallet> walletList = walletRepository.getWalletsByUserId(id);
        List<WalletForUserSummaryDto> walletDtoList = new ArrayList<>();

        WalletToWalletForUserSummaryInfoDtoConverter converter =
                new WalletToWalletForUserSummaryInfoDtoConverter();

        for (Wallet wallet : walletList) {
            WalletForUserSummaryDto walletDto = converter.convert(wallet);
            // Income, Outcome and Balance are counted.
            BigDecimal walletIncome = transactionRepository.getSumIncomeByWallet(wallet.getId());
            BigDecimal walletOutcome = transactionRepository.getSumOutcomeByWallet(wallet.getId());

            walletDto.setIncome(walletIncome.toString());
            walletDto.setOutcome(walletOutcome.toString());
            walletDto.setBalance(walletIncome.subtract(walletOutcome).toString());

            walletDtoList.add(walletDto);
        }

        // TODO: save user`s lastEntrance.

        return new UserSummaryInfoDto(balance.toString(), sumIncome.toString(), sumOutcome.toString(), walletDtoList);
    }
}