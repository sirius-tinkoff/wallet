package sirius.tinkoff.wallet.service;


import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import sirius.tinkoff.wallet.converter.*;
import sirius.tinkoff.wallet.dao.Transaction;
import sirius.tinkoff.wallet.dto.TransactionCreationDto;
import sirius.tinkoff.wallet.dto.TransactionDto;
import sirius.tinkoff.wallet.exception.NoSuchEntityException;
import sirius.tinkoff.wallet.repository.TransactionRepository;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionService {
    private final TransactionRepository transactionRepository;

    private final TransactionDtoToTransactionConverter transactionDtoToTransactionConverter;
    private final TransactionToTransactionDtoConverter transactionToTransactionDtoConverter;
    private final TransactionToTransactionCreationDtoConverter transactionToTransactionCreationDtoConverter;
    private final TransactionCreationDtoToTransactionConverter transactionCreationDtoToTransactionConverter;

    public TransactionDto getById(long id) {
        Transaction transaction = transactionRepository
                .findById(id)
                .orElseThrow(() -> new NoSuchEntityException(
                        format("No transaction with such id (%d).", id)));
        return transactionToTransactionDtoConverter.convert(transaction);
    }

    public TransactionCreationDto create(TransactionCreationDto transactionCreationDto) throws ParseException {
        Transaction transaction =
                transactionCreationDtoToTransactionConverter.convert(transactionCreationDto);
        Transaction saved = transactionRepository.save(transaction);
        return transactionToTransactionCreationDtoConverter.convert(saved);
    }

    public TransactionCreationDto update(TransactionCreationDto transactionCreationDto) throws ParseException {
        Transaction transaction =
                transactionCreationDtoToTransactionConverter.convert(transactionCreationDto);
        transactionRepository.save(transaction);
        return transactionCreationDto;
    }

    public void deleteById(Long id) {
        transactionRepository.findById(id).orElseThrow(() -> new NoSuchEntityException(
                format("Cannot delete. No transaction with such id (%d).", id)));
        transactionRepository.deleteById(id);
    }
}
