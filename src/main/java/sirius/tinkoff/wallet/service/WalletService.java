package sirius.tinkoff.wallet.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import sirius.tinkoff.wallet.converter.*;
import sirius.tinkoff.wallet.dao.Category;
import sirius.tinkoff.wallet.dao.Transaction;
import sirius.tinkoff.wallet.dao.Wallet;
import sirius.tinkoff.wallet.dto.*;
import sirius.tinkoff.wallet.exception.NoSuchEntityException;
import sirius.tinkoff.wallet.repository.CategoryRepository;
import sirius.tinkoff.wallet.repository.TransactionRepository;
import sirius.tinkoff.wallet.repository.UserRepository;
import sirius.tinkoff.wallet.repository.WalletRepository;

import java.math.BigDecimal;
import java.util.*;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
@Slf4j
public class WalletService {
    private final WalletRepository walletRepository;
    private final TransactionRepository transactionRepository;
    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;

    private final WalletToWalletDetailsDtoConverter walletToWalletDetailsDtoConverter;
    private final WalletToWalletCreationDtoConverter walletToWalletCreationDtoConverter;
    private final WalletCreationDtoToWalletConverter walletCreationDtoToWalletConverter;

    public WalletCreationDto getById(Long id) {
        Wallet wallet = walletRepository.findById(id).orElseThrow(() -> new NoSuchEntityException(
                format("No wallet with such id (%d).", id)));
        return walletToWalletCreationDtoConverter.convert(wallet);
    }

    public WalletCreationDto create(WalletCreationDto walletCreationDto) {
        Wallet wallet = walletCreationDtoToWalletConverter.convert(walletCreationDto);
        wallet.setUser(userRepository.getById(walletCreationDto.getUserId()));
        wallet.setCreationDate(new Date());
        Wallet saved = walletRepository.save(wallet);
        return walletToWalletCreationDtoConverter.convert(saved);
    }

    public WalletCreationDto update(WalletCreationDto walletCreationDto) {
        Wallet wallet = walletCreationDtoToWalletConverter.convert(walletCreationDto);
        wallet.setUser(userRepository.getById(walletCreationDto.getUserId()));
        walletRepository.save(wallet);
        return walletCreationDto;
    }

    public void deleteById(Long id) {
        walletRepository.findById(id).orElseThrow(() -> new NoSuchEntityException(
                format("Cannot delete. No wallet with such id (%d).", id)));
        transactionRepository.deleteAllByWalletId(id);
        walletRepository.deleteById(id);
    }

    public WalletDetailsDto getWalletInfoById(Long id) {
        Wallet wallet = walletRepository
                .findById(id)
                .orElseThrow(() -> new NoSuchEntityException(format("No wallet with such id (%d).", id)));

        List<Transaction> transactionList = transactionRepository.getByWalletId(id);

        List<TransactionDto> transactionsInfo = new ArrayList<>();
        TransactionToTransactionDtoConverter transactionConverter = new TransactionToTransactionDtoConverter();
        CategoryToCategoryDtoConverter categoryConverter = new CategoryToCategoryDtoConverter();

        for (Transaction transaction : transactionList) {
            TransactionDto transactionInfo = transactionConverter.convert(transaction);
            Category category = categoryRepository.getById(transaction.getCategoryId());
            CategoryDto categoryDto = categoryConverter.convert(category);
            transactionInfo.setCategory(categoryDto);
            transactionsInfo.add(transactionInfo);
        }

        transactionsInfo.sort((TransactionDto t1, TransactionDto t2) -> t2.getDate().compareTo(t1.getDate()));

        WalletDetailsDto walletDetailsDto = walletToWalletDetailsDtoConverter.convert(wallet);

        BigDecimal income = transactionRepository.getSumIncomeByWallet(id);
        BigDecimal outcome = transactionRepository.getSumOutcomeByWallet(id);

        walletDetailsDto.setIncome(income.toString())
                        .setOutcome(outcome.toString())
                        .setBalance(income.subtract(outcome).toString());

        walletDetailsDto.setTransactions(transactionsInfo);

        return walletDetailsDto;
    }
}
