package sirius.tinkoff.wallet.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@Schema(description = "Пользователь")
public class UserDto {
    private Long id;
    private String googleToken;
    private String email;
}
