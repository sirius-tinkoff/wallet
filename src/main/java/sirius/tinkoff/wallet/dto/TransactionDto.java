package sirius.tinkoff.wallet.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import sirius.tinkoff.wallet.dao.CurrencyType;
import sirius.tinkoff.wallet.dao.TransactionType;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Полная информация о транзакции")
public class TransactionDto {
    private long id;
    private String amount;
    private TransactionType transactionType;
    private CategoryDto category;
    private String date;
    private CurrencyType currencyType;
}