package sirius.tinkoff.wallet.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Schema(description= "Суммы по кошелькам и список кошельков.")
public class UserSummaryInfoDto {
    private String balance;
    private String income;
    private String outcome;
    private List<WalletForUserSummaryDto> walletsList;
}
