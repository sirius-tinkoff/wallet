package sirius.tinkoff.wallet.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import sirius.tinkoff.wallet.dao.TransactionType;

import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
public class CategoryDto {
    private Long id;
    private String name;
    private Long color;
    private Long icon;
    private Date creationDate;
    private TransactionType categoryType;
    private Long userId;
}
