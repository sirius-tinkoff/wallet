package sirius.tinkoff.wallet.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import sirius.tinkoff.wallet.dao.CurrencyType;

import java.math.BigDecimal;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Кошелёк для создания/изменения.")
public class WalletDto {
    private Long id;
    private Long userId;
    private String name;
    private String balance;
    private String income;
    private String outcome;
    private String limit;
    private CurrencyType currencyType;
    private boolean isHidden;
}
