package sirius.tinkoff.wallet.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import sirius.tinkoff.wallet.dao.CurrencyType;

@Setter
@Getter
@Accessors(chain = true)
@Schema(description = "Информация о кошельке без лишнего.")
public class WalletCreationDto {
    private Long id;
    private Long userId;
    private String name;
    private String balance;
    private String limit;
    private CurrencyType currencyType;
    private Boolean hidden;
}
