package sirius.tinkoff.wallet.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import sirius.tinkoff.wallet.dao.CurrencyType;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Информация о кошельке для списка кошельков.")
public class WalletForUserSummaryDto {
    Long id;
    String name;
    String balance;
    String income;
    String outcome;
    String limit;
    CurrencyType currencyType;
    Boolean isHidden;
}
