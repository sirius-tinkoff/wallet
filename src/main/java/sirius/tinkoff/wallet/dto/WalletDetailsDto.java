package sirius.tinkoff.wallet.dto;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import sirius.tinkoff.wallet.dao.CurrencyType;

@Setter
@Getter
@Accessors(chain = true)
@Schema(description = "Детали кошелька")
public class WalletDetailsDto {
    private Long id;
    private String name;
    private String balance;
    private String income;
    private String outcome;
    private String limit;
    private CurrencyType currencyType;
    private Boolean hidden;
    private List<TransactionDto> transactions;
}
