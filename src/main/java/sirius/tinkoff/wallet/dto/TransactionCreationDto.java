package sirius.tinkoff.wallet.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import sirius.tinkoff.wallet.dao.CurrencyType;
import sirius.tinkoff.wallet.dao.TransactionType;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Информация о транзакции для создания.")
public class TransactionCreationDto {
    private Long id;
    private Long walletId;
    private String amount;
    private TransactionType transactionType;
    private Long categoryId;
    private String date;
    private CurrencyType currencyType;
}